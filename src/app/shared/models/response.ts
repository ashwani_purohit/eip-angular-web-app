export interface Response {
    statusCode: string,
    message: string,
    recordCount: number,
    body: any
}
