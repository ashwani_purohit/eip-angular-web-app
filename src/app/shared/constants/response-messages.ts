export class ResponseMessages {
	static Success = 'Success';
	static UserNameAndPasswordAreRequired = 'UserNameAndPasswordAreRequired';
	static UserDoesNotExists = 'UserDoesNotExists';
	static InvalidUserNameOrPassword = 'InvalidUserNameOrPassword';
	static RoleNotAssignedToUser = 'RoleNotAssignedToUser';
	static UsernameOrEmailRegistered = 'UsernameOrEmailRegistered';
	static InputDataNotFound = 'InputDataNotFound';

}
