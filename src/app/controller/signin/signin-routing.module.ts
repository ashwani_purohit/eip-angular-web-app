import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './signin.component';

import { environment } from 'src/environments/environment';
let env = environment;
const home = env.home;
const signinBasePath = home.signin;
const routes: Routes = [
  {
    path: signinBasePath,
    component: SigninComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SigninRoutingModule { }

